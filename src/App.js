import { Routes, Route, Navigate } from 'react-router-dom';
import {useContext} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

import AppNavbar from './component/AppNavbar';

import { UserContext } from './UserContext';

import Login from './pages/Login';
import Register from './pages/Register';
import Todo from './pages/Todo';

function App() {

  const {user} = useContext(UserContext);

  return (
    <>
    <AppNavbar />
    <Container>
      <Routes>
        <Route path="/login" element={user? <Todo /> : <Login />} />
        <Route path="/register" element={user? <Todo /> : <Register />} />
        <Route path="/todo" element={<Todo />} />
      </Routes>
    </Container>
    </>
  );
}

export default App;
