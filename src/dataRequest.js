export const baseUrl = process.env.REACT_APP_URL;

export const postRequest = async (url, body, token) => {

	const response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			 Authorization: `Bearer ${token}`
		},
		body
	})

	const data = await response.json();

	if(!response.ok){
		let message;

		if(data?.message){
			message = data.message;
		}
		else{
			message = data;
		}

		return {error: true, message};
	}

	return data;
}

export const getRequest = async(url) => {
	const response = await fetch(url);

	const data = await response.json()

		if(!response.ok){
			let message = 'Error occurred..'

			if(data?.message){
				message = data.message;
			}

			return {error:true, message}
		}

		return data;
}

export const patchRequest = async (url, body, token) => {
	const response = await fetch(url, {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		},
		body
	})

	const data = await response.json();

	if(!response.ok){
		let message;

		if(data?.message){
			message = data.message;
		}
		else{
			message = data;
		}

		return {error: true, message};
	}

	return data;
}


export const deleteRequest = async(url, body, token) => {
	const response = await fetch(url, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		},
		body

	})

	const data = await response.json()

		if(!response.ok){
			let message = 'Error occurred..'

			if(data?.message){
				message = data.message;
			}

			return {error:true, message}
		}

		return data;
}