import { Container, Nav, Navbar, Stack, Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import {UserContext} from '../UserContext';

// import Icon from '@mdi/react';
// import { mdiMessage } from '@mdi/js';

const AppNavbar = () => {

	const {user, logoutUser} = useContext (UserContext)

	return(
		<Navbar className="mb-4" bg="dark">
			<Container>


				<Nav>
					<Stack direction="horizontal" gap={5}>
						{
							user && (<>
								<Dropdown className="mx-1 mx-md-5">
								    <Dropdown.Toggle className="text-warning" style={{background: 'rgba(0, 0,0,0)', border: 'none'}} id="dropdown-basic">{user?.name}</Dropdown.Toggle>
								    <Dropdown.Menu>
								       	<Dropdown.Item as={Link} to="/profile">Profile</Dropdown.Item>
								       	<Dropdown.Divider />
								       	<Dropdown.Item as={Link} onClick={() =>logoutUser()} to="/login">Logout</Dropdown.Item>
								    </Dropdown.Menu>
								</Dropdown>
								</>
							)
						}
						{!user && (<>
							<Link to="/login" className="link-light text-decoration-none">Login</Link>
							<Link to="/register" className="link-light text-decoration-none">Register</Link>
						</>)}
					</Stack>
				</Nav>
			</Container>
		</Navbar>
	)
}

export default AppNavbar;