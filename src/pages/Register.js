import {useContext} from 'react';
import {Alert, Button, Form, Row, Col, Stack } from 'react-bootstrap';
import {UserContext} from '../UserContext';

const Register = () => {

	const {registerInfo, updateRegisterInfo, registerUser, registerError, isRegisterLoading} = useContext(UserContext);

	return(
		<>
		<Stack className="mx-auto col-9 col-md-6 col-lg-6">
			<Form onSubmit={registerUser}>
				<Row 
					style={{
						justifyContent: 'center',
						paddingTop: '20%'
					}}
				>
					<Col>
						<Stack gap={3}>
							<h1 style={{fontWeight: 'bold'}}>Register</h1>
							<Form.Control type="text" placeholder="Name" onChange={(e) => updateRegisterInfo({
								...registerInfo, name: e.target.value
							})}/>
							<Form.Control type="email" placeholder="Email" onChange={(e) => updateRegisterInfo({
								...registerInfo, email: e.target.value
							})}/>
							<Form.Control type="password" placeholder="Password" onChange={(e) => updateRegisterInfo({
								...registerInfo, password: e.target.value
							})}/>
							<Button style={{backgroundColor: '#074EBB'}} type="submit">{isRegisterLoading? 'Creating account' : 'Register'}</Button>
							{
								registerError?.error &&  <Alert variant="danger">{registerError?.message}</Alert>
							}
							
						</Stack>
					</Col>
				</Row>
			</Form>
		</Stack>
		</>
	)
}

export default Register;