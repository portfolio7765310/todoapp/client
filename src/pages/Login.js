import {Alert, Button, Form, Row, Col, Stack } from 'react-bootstrap';
import {UserContext} from '../UserContext';
import { useContext } from 'react';
import {useNavigate} from 'react-router-dom'


export default function Login (){

	const {loginUser, loginError, loginInfo, updateLoginInfo, isLoginLoading} = useContext(UserContext);


	const navigate = useNavigate();

	return(
		<>
		<Stack className="mx-auto col-9 col-md-6 col-lg-6">
			<Form onSubmit={loginUser}>
				<Row 
					style={{
						justifyContent: 'center',
						paddingTop: '20%'
					}}
				>
					<Col>
						<Stack gap={3}>
							<h1 style={{fontWeight: 'bold'}}>Login</h1>
							<Form.Control type="email" placeholder="Email" onChange={(e) => updateLoginInfo({...loginInfo, email: e.target.value})}/>
							<Form.Control type="password" placeholder="Password" onChange={(e) => updateLoginInfo({...loginInfo, password: e.target.value})}/>
							<Button style={{backgroundColor: '#074EBB'}} type="submit">{isLoginLoading? 'Signing in..' : 'Login'}</Button>
							{loginError?.error && 
							<Alert variant="danger">{loginError?.message}!</Alert>}
						</Stack>
						<div className="mt-2 text-center">
							No account yet? <button style={{border: 'none', background: 'none', fontWeight: 'bold'}} onClick={()=>navigate('/register')}>Sign-up here</button>
						</div>
					</Col>

				</Row>
			</Form>
		</Stack>
		</>
	)
}