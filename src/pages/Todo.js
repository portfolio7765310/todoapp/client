import {Stack, Form, Button, Row, Col, Modal, Card} from 'react-bootstrap';
import { useContext, useEffect, useState } from 'react';
import { baseUrl, postRequest, getRequest, patchRequest, deleteRequest } from '../dataRequest';
import { UserContext } from '../UserContext';
import Icon from '@mdi/react';
import {mdiPencil, mdiDelete} from '@mdi/js';

export default function Todo () {
	const {user} = useContext(UserContext);
	// console.log(user)
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [status, setStatus] = useState(false);
	const [taskUpdated, setTaskUpdated] = useState('');

	const [tasksList, setTasksList] = useState('');

	const [editingTaskId, setEditingTaskId] = useState();

	const [show, setShow] = useState(false);
	const [showDelete, setShowDelete] = useState(false);
	const [newTitle, setNewTitle] = useState('');
	const [newDescription, setNewDescription]= useState('');


	const handleClose = () => {
		setShow(false);
		setShowDelete(false);
	}

	const handleShow = (taskId) => {
		setShow(true);
		setEditingTaskId(taskId);
	};

	const handleDelete = (taskId) => {
		setShowDelete(true)
		setEditingTaskId(taskId);
	}

	const displayTasks = async (e) => {
	  const response = await getRequest(`${baseUrl}/users/find/${user._id}`)
	  const tasksList = response.map((task) => (
	  	
	    <div key={task._id}>
	    	<Card className="mt-2 text-center">
		      	<h2>{task.title}</h2>
		      	<p>{task.description}</p>
		      	<div>
			      	<Icon path={mdiPencil} size={1} role="button" onClick={()=>handleShow(task._id)} className="mx-4"/>
			      	<Icon path={mdiDelete} size={1} role="button" onClick={()=>handleDelete(task._id)} className="mx-4"/>
		      	</div>
	      	</Card>
	    </div>
	    
	  ))
	  setTasksList(tasksList)
	  console.log(tasksList)
	}

	const editTask = async(e) => {
		e.preventDefault();
		const taskId = e.target.dataset.taskId;
		const response = await patchRequest(`${baseUrl}/tasks/edit/${editingTaskId}`, JSON.stringify({
			title: newTitle,
			description: newDescription
		}), user.token)
		handleClose();
		await displayTasks()
	}

	const addTask = async(e) => {
	  e.preventDefault();
	  const payload = JSON.stringify({
	  	    title: title,
	  	    description: description
	  	  });
	  const response = await postRequest(`${baseUrl}/tasks/create`, payload, user.token);

	  console.log(response); // log the response to see what it looks like
	  if(response.error){
	    return (response);
	  }

	  setTaskUpdated(response);
	  setTitle('');
	  setDescription('');
	  displayTasks();
	}


	const deleteTask = async(e) => {
		e.preventDefault();

		const taskId = e.target.dataset.taskId;
		const response = await deleteRequest(`${baseUrl}/tasks/delete/${editingTaskId}`, JSON.stringify(taskId), user.token)
		handleClose();
		await displayTasks()
	}





	useEffect(()=>{
		displayTasks();
	}, [])

	return(
		<>
		<Stack className="mx-auto col-9 col-md-6 col-lg-6">
			<Form onSubmit={addTask}>
				<Row 
					style={{
						justifyContent: 'center',
						paddingTop: '20%'
					}}
				>
					<Col>
						<Stack gap={3}>
							<h1 style={{fontWeight: 'bold'}}>To Do List</h1>
							<Form.Control 
								type="text" 
								placeholder="What is your task today?"
								onChange={e => setTitle(e.target.value)}
								value={title}
							/>
							<Form.Control 
								type="text" 
								placeholder="Add a short description" 
								onChange={e => setDescription(e.target.value)}
								value={description}
							/>
							<Button variant="dark" type="submit">Add task</Button>
						</Stack>
					</Col>

				</Row>
			</Form>
			<Stack>
				{tasksList}
			</Stack>
		</Stack>


	      <Modal show={show} onHide={handleClose}>
	        <Modal.Header closeButton>
	          <Modal.Title>Edit task</Modal.Title>
	        </Modal.Header>
	        <Modal.Body>
	          <Form>
	            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	              <Form.Label>Title</Form.Label>
	              <Form.Control
	                type="email"
	                placeholder="Update title"
	                autoFocus
	                onChange={e => setNewTitle(e.target.value)}
	              />
	            </Form.Group>
	            <Form.Group
	              className="mb-3"
	              controlId="exampleForm.ControlTextarea1"
	            >
	              <Form.Label>Description</Form.Label>
	              <Form.Control 
	              	as="textarea" 
	              	rows={3} 
	              	placeholder="New description"
	              	onChange={e => setNewDescription(e.target.value)}
	              />
	            </Form.Group>
	          </Form>
	        </Modal.Body>
	        <Modal.Footer>
	          <Button variant="secondary" onClick={handleClose}>
	            Close
	          </Button>
	          <Button variant="primary" onClick={editTask} >
	            Save Changes
	          </Button>
	        </Modal.Footer>
	      </Modal>



	      <Modal show={showDelete} onHide={handleClose}>
	        <Modal.Header closeButton>
	          <Modal.Title>Delete this task?</Modal.Title>
	        </Modal.Header>
	        <Modal.Body className="text-center">
	          <Button variant="secondary" onClick={handleClose}className="mx-3">
	            Close
	          </Button>
	          <Button variant="danger" onClick={deleteTask} className="mx-3">
	            Delete
	          </Button>
	        </Modal.Body>
	        <Modal.Footer>

	        </Modal.Footer>
	      </Modal>
	    </>
	)
}